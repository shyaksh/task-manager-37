package ru.bokhan.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.*;
import ru.bokhan.tm.dto.TaskDto;

@RequestMapping("/api/task")
public interface ITaskRestEndpoint {

    @NotNull
    @RequestMapping(method = {RequestMethod.POST, RequestMethod.PUT})
    TaskDto save(final TaskDto s);

    @Nullable
    @GetMapping("/${id}")
    TaskDto findById(@PathVariable("id") final String id);

    @GetMapping("/exists/${id}")
    boolean existsById(@PathVariable("id") final String id);

    @DeleteMapping("/${id}")
    void deleteById(@PathVariable("id") final String id);

}
