package ru.bokhan.tm.repository.dto;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.bokhan.tm.dto.AbstractDto;

public interface AbstractDtoRepository<D extends AbstractDto> extends JpaRepository<D, String> {

}
