package ru.bokhan.tm.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("ru.bokhan.tm")
public class ApplicationConfiguration {

}
