package ru.bokhan.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.bokhan.tm.api.endpoint.ITaskRestEndpoint;
import ru.bokhan.tm.dto.TaskDto;
import ru.bokhan.tm.repository.dto.TaskDtoRepository;

@RestController
@RequestMapping("/api/task")
public class TaskRestEndpoint implements ITaskRestEndpoint {

    @Autowired
    private TaskDtoRepository taskDtoRepository;

    @NotNull
    @Override
    @RequestMapping(method = {RequestMethod.POST, RequestMethod.PUT})
    public TaskDto save(@NotNull @RequestBody final TaskDto s) {
        return taskDtoRepository.save(s);
    }

    @Nullable
    @Override
    @GetMapping("/{id}")
    public TaskDto findById(@NotNull @PathVariable("id") final String id) {
        return taskDtoRepository.findById(id).orElse(null);
    }

    @Override
    @GetMapping("/exists/{id}")
    public boolean existsById(@NotNull @PathVariable("id") final String id) {
        return taskDtoRepository.existsById(id);
    }

    @Override
    @DeleteMapping("/{id}")
    public void deleteById(@NotNull @PathVariable("id") final String id) {
        taskDtoRepository.deleteById(id);
    }

}
