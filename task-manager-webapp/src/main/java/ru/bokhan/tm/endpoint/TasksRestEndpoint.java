package ru.bokhan.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.bokhan.tm.api.endpoint.ITasksRestEndpoint;
import ru.bokhan.tm.dto.TaskDto;
import ru.bokhan.tm.repository.dto.TaskDtoRepository;

import java.util.List;

@RestController
@RequestMapping("/api/tasks")
public class TasksRestEndpoint implements ITasksRestEndpoint {

    @Autowired
    private TaskDtoRepository taskDtoRepository;

    @NotNull
    @Override
    @GetMapping
    public List<TaskDto> findAll() {
        return taskDtoRepository.findAll();
    }

    @NotNull
    @Override
    @RequestMapping(method = {RequestMethod.POST, RequestMethod.PUT})
    public List<TaskDto> saveAll(@NotNull @RequestBody final List<TaskDto> list) {
        return taskDtoRepository.saveAll(list);
    }

    @NotNull
    @Override
    @GetMapping("/count")
    public Long count() {
        return taskDtoRepository.count();
    }

    @Override
    @PostMapping("/delete")
    public void deleteAll(@NotNull @RequestBody final List<TaskDto> list) {
        taskDtoRepository.deleteAll(list);
    }

    @Override
    @DeleteMapping("/all")
    public void deleteAll() {
        taskDtoRepository.deleteAll();
    }

}
