package ru.bokhan.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import ru.bokhan.tm.dto.UserDTO;

@Repository
public interface UserRepositoryDTO extends AbstractRepositoryDTO<UserDTO> {

    UserDTO findByLogin(@NotNull final String login);

}

