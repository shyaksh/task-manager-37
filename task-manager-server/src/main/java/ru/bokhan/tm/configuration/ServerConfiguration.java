package ru.bokhan.tm.configuration;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@ComponentScan("ru.bokhan.tm")
@EnableJpaRepositories("ru.bokhan.tm.repository")
@PropertySource("classpath:application.properties")
public class ServerConfiguration {

    @Bean
    @NotNull
    public DataSource dataSource(
            @NotNull @Value("${jdbc.driver}") final String jdbcDriver,
            @NotNull @Value("${jdbc.url}") final String jdbcUrl,
            @NotNull @Value("${jdbc.login}") final String jdbcLogin,
            @NotNull @Value("${jdbc.password}") final String jdbcPassword
    ) {
        @NotNull final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(jdbcDriver);
        dataSource.setUrl(jdbcUrl);
        dataSource.setUsername(jdbcLogin);
        dataSource.setPassword(jdbcPassword);
        return dataSource;
    }

    @Bean
    @NotNull
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(
            @NotNull final DataSource dataSource,
            @Value("${hibernate.show_sql}") final boolean showSql,
            @NotNull @Value("${hibernate.hbm2ddl.auto}") final String tableStrategy,
            @NotNull @Value("${hibernate.dialect}") final String dialect
    ) {
        @NotNull final LocalContainerEntityManagerFactoryBean factoryBean =
                new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan("ru.bokhan.tm.entity", "ru.bokhan.tm.dto");
        @NotNull final Properties properties = new Properties();
        properties.put("hibernate.show_sql", showSql);
        properties.put("hibernate.hbm2ddl.auto", tableStrategy);
        properties.put("hibernate.dialect", dialect);
        factoryBean.setJpaProperties(properties);
        return factoryBean;
    }

    @Bean
    @NotNull
    public PlatformTransactionManager transactionManager(
            @NotNull final LocalContainerEntityManagerFactoryBean entityManagerFactory
    ) {
        @NotNull final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory.getObject());
        return transactionManager;
    }

}
