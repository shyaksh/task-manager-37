package ru.bokhan.tm.listener.data.xml;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.bokhan.tm.api.ICurrentSessionService;
import ru.bokhan.tm.endpoint.DomainEndpoint;
import ru.bokhan.tm.endpoint.SessionDTO;
import ru.bokhan.tm.event.ConsoleEvent;
import ru.bokhan.tm.exception.security.AccessDeniedException;
import ru.bokhan.tm.listener.AbstractListener;

@Component
public final class DataXmlLoadListener extends AbstractListener {

    @NotNull
    @Autowired
    private DomainEndpoint domainEndpoint;

    @NotNull
    @Autowired
    private ICurrentSessionService currentSessionService;

    @NotNull
    @Override
    public String command() {
        return "data-xml-load";
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Load data from xml file.";
    }

    @Override
    @EventListener(condition = "@dataXmlLoadListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        @Nullable final SessionDTO session = currentSessionService.getCurrentSession();
        if (session == null) throw new AccessDeniedException();
        System.out.println("[DATA XML LOAD]");
        if (domainEndpoint.loadFromXml(session)) System.out.println("[OK]");
    }

}
